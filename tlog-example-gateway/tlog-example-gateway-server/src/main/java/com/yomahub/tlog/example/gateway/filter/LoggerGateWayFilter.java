package com.yomahub.tlog.example.gateway.filter;

import com.google.common.base.Stopwatch;
import com.yomahub.tlog.context.TLogContext;
import com.yomahub.tlog.webflux.common.TLogWebFluxCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Supplier;


/**
 * 日志打印
 */
@Component
public class LoggerGateWayFilter implements GlobalFilter, Ordered {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return chain.filter(exchange).then(
                Mono.fromSupplier(() -> {
                    log.info("test");
                    return null;
                }));
    }

    @Override
    public int getOrder() {
        return -3;
    }
}
